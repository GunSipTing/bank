from django.contrib import admin

# Register your models here.
from .models import Order_Line_Item


admin.site.register(Order_Line_Item)