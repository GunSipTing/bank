from django.db import models
from product.models import Product
# Create your models here.
class Order_Line_Item(models.Model):
    #date = models.DateTimeField()
    #status = models.ForeignKey(Status)
    # delivery , 
    pid = models.ForeignKey(Product)
    quantity = models.IntegerField()
    oid = models.ForeignKey('order.Order')


    def price(self):
        return self.pid.price*self.quantity
  
    def __str__(self):
        return "eiei"
