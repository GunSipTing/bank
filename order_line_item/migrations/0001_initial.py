# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0005_product_stock'),
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order_Line_Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField()),
                ('oid', models.ForeignKey(to='order.Order')),
                ('pid', models.ForeignKey(to='product.Product')),
            ],
        ),
    ]
