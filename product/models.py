from django.db import models

# Create your models here.

class Category(models.Model):
	name = models.CharField(max_length=50)

	def __unicode__(self):
		return self.name

class Brand(models.Model):
	name = models.CharField(max_length=100)
	
	def __unicode__(self):
		return self.name

class Image(models.Model):
	name = models.CharField(max_length=100)
	img  = models.URLField(max_length=1000)
	
	def __unicode__(self):
		return self.name

class Product(models.Model):

	name = models.CharField(max_length=100)
	description = models.URLField(max_length=1000)
	category = models.ForeignKey(Category)
	brand = models.ForeignKey(Brand,null=True)
	price = models.FloatField()
	main_image = models.URLField(max_length=1000)
	img = models.ManyToManyField(Image)
	stock = models.IntegerField()
	
	def __unicode__(self):
		return ("%d : %s : %s : %s : %.2f")%(self.stock,self.name,self.brand.name,self.category.name,self.price) 






	
