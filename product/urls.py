from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^box/$', views.product_box, name='product_box'),
    url(r'^(?P<id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^list/$', views.list,name='list'),
    url(r'^cat/$', views.cat,name='cat'),
]
