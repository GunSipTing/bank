from django.shortcuts import render
from django.http import HttpResponse
from .models import Product
from .models import Category
from .models import Brand
from django.template import RequestContext, loader
from ting import *
# Create your views here.

def cat(request):

    cat = get_all_category()
    return render(request, 'product/cat.html', {'cat':cat})

def product_box(request):
    
    #print request.GET
    #print Product.objects.all()
    l = get_product_list(request)[0]
    l = l.order_by('-stock')
    #l = l.objects.filunicode(ter()brand__name=request.GET["Brand"])
    context = {'list': l}
    #print l
    return render(request, 'product/product_box.html', context)

def list(request):
    
    #print Product.objects.all()
    l,text = get_product_list(request)
    l = get_product_list(request)[0]
    #l = l.order_by('-stock')
    
    #l = l.objects.filunicode(ter()brand__name=request.GET["Brand"])
    context = {'list': l , 
                'cat':get_all_category(),
                'text':text,
                }

    #print l
    return render(request, 'product/list.html', context)

def detail(request, id):
    item = Product.objects.filter(id=id)[0]
    context = {'item': item,'cat': get_all_category()}

    return render(request, 'product/detail.html', context)

def home(request):

    cat = get_all_category()
#    for i,j in cat:
#        print i,j

    context = {'cat': cat}
    return render(request, 'home.html',context)

def get_product_list(request):

    text = []
    
    l = Product.objects.all()
    #print unicode(l[0]).lower()
    if "Category" in request.GET:
        l = l.filter(category__name=request.GET["Category"])
        text += [request.GET["Category"]]
    if "Brand" in request.GET:
        l = l.filter(brand__name=request.GET["Brand"])
        text += [request.GET["Brand"]]
    if "Search" in request.GET:
        text += ["Search : "+request.GET["Search"].lower()]
        l = [i for i in l if request.GET["Search"].lower() in unicode(i).lower()]
    return l," , ".join(text)

