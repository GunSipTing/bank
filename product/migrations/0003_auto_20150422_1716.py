# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_auto_20150417_0753'),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('img', models.URLField(max_length=1000)),
            ],
        ),
        migrations.RenameField(
            model_name='product',
            old_name='type',
            new_name='category',
        ),
        migrations.RemoveField(
            model_name='product',
            name='main_img',
        ),
        migrations.AddField(
            model_name='product',
            name='img',
            field=models.ManyToManyField(to='product.Image'),
        ),
    ]
