# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0003_auto_20150504_2132'),
    ]

    operations = [
        migrations.AddField(
            model_name='custumuser',
            name='phone_number',
            field=models.IntegerField(null=True),
        ),
    ]
