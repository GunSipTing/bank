# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0002_auto_20150504_1629'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='custumuser',
            name='Address',
        ),
        migrations.AddField(
            model_name='custumuser',
            name='address',
            field=models.CharField(max_length=10000, null=True),
        ),
        migrations.AddField(
            model_name='custumuser',
            name='address_extra',
            field=models.CharField(max_length=10000, null=True),
        ),
        migrations.AddField(
            model_name='custumuser',
            name='zip_code',
            field=models.IntegerField(null=True),
        ),
    ]
