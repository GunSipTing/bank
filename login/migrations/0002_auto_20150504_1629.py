# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='following',
            name='user',
        ),
        migrations.RemoveField(
            model_name='following',
            name='user_follow',
        ),
        migrations.RemoveField(
            model_name='custumuser',
            name='win',
        ),
        migrations.AddField(
            model_name='custumuser',
            name='Address',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='custumuser',
            name='avatar_img',
            field=models.URLField(null=True),
        ),
        migrations.DeleteModel(
            name='Following',
        ),
    ]
