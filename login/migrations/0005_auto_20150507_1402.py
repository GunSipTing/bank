# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('login', '0004_custumuser_phone_number'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=50, null=True)),
                ('surname', models.CharField(max_length=50, null=True)),
                ('address', models.CharField(max_length=150, null=True)),
                ('address_extra', models.CharField(max_length=50, null=True)),
                ('province', models.CharField(max_length=50, null=True)),
                ('zip_code', models.IntegerField(null=True)),
                ('phone_number', models.CharField(max_length=10, null=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='custumuser',
            name='address_extra',
        ),
        migrations.RemoveField(
            model_name='custumuser',
            name='phone_number',
        ),
        migrations.RemoveField(
            model_name='custumuser',
            name='zip_code',
        ),
        migrations.AlterField(
            model_name='custumuser',
            name='address',
            field=models.ForeignKey(to='login.UserAddress', null=True),
        ),
    ]
