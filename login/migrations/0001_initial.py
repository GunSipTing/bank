# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CustumUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('avatar_img', models.URLField()),
                ('display_name', models.CharField(max_length=50, null=True)),
                ('win', models.IntegerField(default=0)),
                ('user', models.ForeignKey(related_name='Auser', to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Following',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('user', models.ForeignKey(related_name='follower', to='login.CustumUser', null=True)),
                ('user_follow', models.ForeignKey(related_name='following', to='login.CustumUser', null=True)),
            ],
        ),
    ]
