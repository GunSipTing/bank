from django.db import models
from django.forms import ModelForm
from django.db.models import Q
from django.contrib.auth.models import User

# Create your models here.
class UserAddress(models.Model):
    first_name = models.CharField(null=True,max_length=50)
    surname = models.CharField(null=True,max_length=50)
    address = models.CharField(null=True, max_length=150)
    address_extra = models.CharField(null=True, max_length=50)
    province = models.CharField(null=True, max_length=50)
    zip_code = models.IntegerField(null=True)
    phone_number = models.CharField(null=True, max_length=10)

    def __unicode__(self):
        #return "%s"%(self.first_name)
        return "%s @ %s"%(self.first_name,self.phone_number)

class CustumUser(models.Model):
    """
    Description: Model Description
    """

    user = models.ForeignKey(User,null=True,related_name='Auser')
    avatar_img = models.URLField(null=True)
    display_name = models.CharField(max_length=50,null=True)
    address = models.ForeignKey(UserAddress,null=True)

    def __unicode__(self):
        return "%d : %s"%(self.id,self.display_name)


class CustumUserForm(ModelForm):
    class Meta:
        model = UserAddress
        fields = ['first_name','surname','address','address_extra','province','zip_code','phone_number']