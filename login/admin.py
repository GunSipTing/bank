from django.contrib import admin

# Register your models here.
from models import CustumUser,UserAddress

admin.site.register(CustumUser)
admin.site.register(UserAddress)