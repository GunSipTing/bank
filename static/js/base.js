var load = function(url,callback,param,success){

    if( !param )
    {
        param = {}
    }
    $.get( url, param ,function( data ) {
      $(callback).html( data );
      if(success)
        success();
 
    });
}

var send_cart = function()
{
	load("/cart","#cart-dropdown",{'Cart':get_cart().toString()});
}

var get_cart = function()
{
	if(!localStorage.bank_cart)
		cart = []
	else
		cart = JSON.parse(localStorage.bank_cart);

	return cart;
}

var save_cart = function(cart)
{
	localStorage.bank_cart = JSON.stringify(cart);
	send_cart()
}

var add_to_cart = function(id)
{
	Cart = get_cart()
	Cart.push(id)
	save_cart(Cart)
}

var del_item_cart = function(id)
{
	Cart = get_cart()
	var index = Cart.indexOf(id);
	if(index!=-1)
   		Cart.splice(index, 1);
   	save_cart(Cart)
	
}

var clear_cart = function()
{
	Cart = []
	save_cart(Cart)
}

var checkout = function()
{
	$.get("/checkout",{'Cart':get_cart().toString()},
		function(resp) {
			if(!(resp==0))
			{
				clear_cart();
				window.location.href = "/history";
			}
			else
			{
				window.location.href = "/member";
			}
			alert(resp); 
		});
}
