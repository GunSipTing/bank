from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'bank_.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^product/', include('product.urls')),
    url(r'^login/', include('login.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'product.views.home',name="home_page"),
    url(r'^logout/$', 'login.views.logout',name="logout"),
    url(r'^member/$', 'bank_.views.member',name="member"),
    url(r'^cart/$','order.views.cart',name="cart"),
    url(r'^checkout/$','order.views.order_from_cart',name="checkout"),
    url(r'^history/$','order.views.order_list',name="history"),
    url(r'^pos/$','order.views.pos',name="pos"),
    url(r'^pay/$','order.views.pay',name="pay"),
    url(r'^order/$','order.views.get_order',name="get_order"),
    url(r'^ship/$','order.views.ship',name="ship"),
          
]
