from login.models import CustumUserForm,CustumUser,UserAddress


from django.shortcuts import render_to_response,HttpResponse, redirect,render
from django.template.context import RequestContext


from django.contrib.auth.decorators import login_required
from ting import *
from django.db.models import Q


@login_required
def member(request):

    user = get_custom_user(request.user)
    

    if request.method == "POST":


        form = CustumUserForm(request.POST,instance=user.address)
        if form.is_valid():

            # commit=False means the form doesn't save at this time.
            # commit defaults to True which means it normally saves.
            model_instance = form.save(commit=False)


            model_instance.save()
            

            user.address = model_instance
            user.save()

            return redirect('/member')
    else:
        form = CustumUserForm(instance=user.address)

    return render(request, "member.html", {'form': form , "cat": get_all_category()})