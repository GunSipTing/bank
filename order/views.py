from django.shortcuts import render

# Create your views here.
from login.models import CustumUserForm,CustumUser


from django.shortcuts import render_to_response,HttpResponse, redirect,render
from django.template.context import RequestContext


from django.contrib.auth.decorators import login_required
from ting import *
from django.db.models import Q
from order.models import Order
from order.models import Status
from order_line_item.models import Order_Line_Item
from product.models import Product
from .forms import PosForm

@login_required
def pos(request):

    #current_user = get_custom_user(request.user)

    orders = Order.objects.all()

    inord = orders.filter(status__name="In Order").order_by('-date')
    paid = orders.filter(status__name="Paid").order_by('-date')
    shipped = orders.filter(status__name="Shipped").order_by('-date')

    orders = list(inord) + list(paid) + list(shipped)

    fout = ""
    total = 0

    #print current_user
    
    if request.method == 'POST':
    # create a form instance and populate it with data from the request:
        form = PosForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            data = form.cleaned_data
            #print data
            inord = inord.filter(date__range=[data['start'],data['end']])
            paid = paid.filter(date__range=[data['start'],data['end']])
            shipped = shipped.filter(date__range=[data['start'],data['end']])

            orders = list(inord) + list(paid) + list(shipped)
            
            #fout = data['start']# +" to "+ form['end'].value()
            #print data['start']
            #print data['end']
        # if a GET (or any other method) we'll create a blank form
    else:
        form = PosForm()

    total = sum(map(lambda x : x.total_price() , list(paid) + list(shipped)))

    #cat = [ (i,Brand.objects.filter(product__category__name=i.name).distinct()) for i in cat]
    context = {'fout':fout, 'orders': orders,'cat':get_all_category(), 'form': form , 'total':total}
    return render(request, 'pos.html',context)

@login_required
def order_list(request):
    current_user = get_custom_user(request.user)

    orders = Order.objects.filter(user=current_user)

    inord = orders.filter(status__name="In Order").order_by('-date')
    paid = orders.filter(status__name="Paid").order_by('-date')
    shipped = orders.filter(status__name="Shipped").order_by('-date')

    orders = list(inord) + list(paid) + list(shipped)
    #cat = [ (i,Brand.objects.filter(product__category__name=i.name).distinct()) for i in cat]
    context = {'orders': orders,'cat':get_all_category()}
    return render(request, 'history.html',context)
@login_required
def get_order(request):
    orders = Order.objects.all()
    orders = orders.get(id=request.GET["id"])
    
    olim = Order_Line_Item.objects.filter(oid=orders)

    context = {'o': orders,'A': orders.user.address, 'ol': olim , 'cat':get_all_category()}
    #print olim
    return render(request, 'get_order.html', context)

@login_required
def ship(request):
    obj = Order.objects.filter(id=int(request.GET["id"]))
    if len(obj) == 0:
        return HttpResponse("False")
    obj[0].status = Status.objects.filter(name="Shipped")[0] 
    obj[0].save()     
    return HttpResponse("Success")

@login_required
def pay(request):

    obj = Order.objects.filter(id=int(request.GET["id"]))
    if len(obj) == 0:
        return HttpResponse("False")

    obj[0].status = Status.objects.filter(name="Paid")[0]
    olim = Order_Line_Item.objects.filter(oid=obj[0])
    obj[0].save()
    for olitem in olim:

        product = Product.objects.all()
        product = product.get(id=olitem.pid.id)
        #print olitem.pid.id,olitem.quantity,product.stock
        product.stock -= olitem.quantity
        product.save()
        #print olitem.pid.id,olitem.quantity,product.stock
     
    return HttpResponse("Success")

def order_from_cart(request):

    current_user = get_custom_user(request.user)
    if current_user.address == None:
        return HttpResponse("0")
    order = Order(user=current_user)
    order.status = Status.objects.get(name="In Order")
    order.save()

    cart = get_cart(request)

    for quantity,product in cart:

        olim = Order_Line_Item()
        olim.pid = product
        olim.quantity = quantity
        olim.oid = order
        olim.save()

    return HttpResponse(order.id)

#@login_required
def cart(request):

    #user = get_custom_user(request.user)

    #print request.GET["Cart"]

    #if request.method == 'POST':
        #print 'Raw Data: "%s"' % request.body   

    cart = get_cart(request) 
    #print cart   
        
    #cat = [ (i,Brand.objects.filter(product__category__name=i.name).distinct()) for i in cat]
    total = sum(map(lambda x: x[0]*x[1].price , cart))
    #print total
    context = {'cart': cart, 'total' : total }

    return render(request, 'cart.html',context)
#[1,2,3] >  

@login_required
def home(request):

    cat = Category.objects.all()
    cat = [ (i,Brand.objects.filter(product__category__name=i.name).distinct()) for i in cat]

#    for i,j in cat:
#        print i,j

    context = {'cat': cat}
    return render(request, 'home.html',context)

#@login_required
def get_cart(request):

    #print request.GET
    if not request.GET["Cart"] :
        return []
    l = Product.objects.all()
    lcount = map(int,request.GET["Cart"].split(','))
    lst = list(set(lcount))
    cart = []
    #cart = [(3,l.get(id=int(i))) for i in request.GET["Cart"].split(',')]
    for i in lst : 
        x = l.filter(id=int(i))
        #print x
        if len(x) == 0:
            continue
        y = lcount.count(i)
        cart.append((y,x[0]))

    return cart
