from django.db import models

from login.models import CustumUser
from order_line_item.models import Order_Line_Item
# Create your models here.
class Status(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name


class Order(models.Model):
    date = models.DateTimeField(auto_now=True)
    status = models.ForeignKey(Status)
    user = models.ForeignKey(CustumUser,null=True)  

    def total_price(self):
                
        return sum(map(lambda x: x.price(),Order_Line_Item.objects.filter(oid=self)))

    def __unicode__(self):
        #return str(self.id)
        if self.user.address:
            return "%s"%(self.user.address) 
        return "%s"%(self.user)
        