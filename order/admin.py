from django.contrib import admin

# Register your models here.
from .models import Status,Order


admin.site.register(Status)
admin.site.register(Order)
