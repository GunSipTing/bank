from django import forms
from datetimewidget.widgets import DateTimeWidget, DateWidget, TimeWidget 

dateTimeOptions = {
	'format': 'dd/mm/yyyy HH:ii P',
	'autoclose': True,
	'showMeridian' : True
}
class PosForm(forms.Form):
    start = forms.DateTimeField(input_formats=['%Y-%m-%d %H:%M:%S'],widget=DateTimeWidget(usel10n=True, bootstrap_version=3,options = dateTimeOptions))
    end = forms.DateTimeField(input_formats=['%Y-%m-%d %H:%M:%S'],widget=DateTimeWidget(usel10n=True, bootstrap_version=3,options = dateTimeOptions))